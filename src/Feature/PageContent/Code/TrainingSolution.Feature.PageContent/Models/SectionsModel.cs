﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrainingSolution.Feature.PageContent.Models
{
    public class SectionsModel
    {
        public string Title { get; set; }
        public string Link { get; set; }
        public string Image { get; set; }
        public string AltText { get; set; }
    }
}