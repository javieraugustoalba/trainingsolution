﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrainingSolution.Feature.PageContent.Models;
using TrainingSolution.Feature.PageContent.Services;

namespace TrainingSolution.Feature.PageContent.Controllers
{
    public class PageContentController : Controller
    {
        private readonly IPageContentService _iPageContentService;
        public PageContentController(IPageContentService iPageContentService)
        {
            _iPageContentService = iPageContentService;
        }
        public ActionResult Sections()
        {
            var model = _iPageContentService.GetSections();
            return View(model);
        }
    }
}