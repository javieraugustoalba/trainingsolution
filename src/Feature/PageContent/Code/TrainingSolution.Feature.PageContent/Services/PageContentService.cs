﻿using Sitecore.Foundation.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TrainingSolution.Feature.PageContent.Models;
using TrainingSolution.Feature.PageContent.Repositories;
using TrainingSolution.Feature.PageContent.Services;

namespace TrainingSolution.Feature.PageContent.Services
{
    [Service(typeof(IPageContentService))]
    public class PageContentService : IPageContentService
    {
        private readonly IPageContentRepository _pageContentRepository;

        public PageContentService(IPageContentRepository pageContentRepository)
        {
            _pageContentRepository = pageContentRepository;
        }
        public List<SectionsModel> GetSections()
        {
            var model = new List<SectionsModel>();
            try
            {
                model = _pageContentRepository.GetSections();
            }
            catch (Exception ex)
            {

            }
            return model;
        }
    }
}