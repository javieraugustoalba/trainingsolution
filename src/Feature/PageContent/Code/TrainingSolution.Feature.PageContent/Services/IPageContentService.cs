﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TrainingSolution.Feature.PageContent.Models;

namespace TrainingSolution.Feature.PageContent.Services
{
    public interface IPageContentService 
    {
        List<SectionsModel> GetSections();
    }
}