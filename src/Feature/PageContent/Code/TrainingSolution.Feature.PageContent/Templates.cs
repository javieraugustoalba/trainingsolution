﻿using Sitecore.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrainingSolution.Feature.PageContent
{
    public class Templates
    {
        public struct Section
        {
            public static readonly ID ID = new ID("{D8ED00BB-DE81-4730-8F31-C913CD6546B5}");
            public struct Fields
            {
                public static readonly ID TitleID = new ID("{DDC2577F-3886-4B46-B353-3FF31F1D3B39}");
                public static readonly string Title = "Title";
                public static readonly ID LinkID = new ID("{83D7038D-A658-4576-A341-5813EBC3A65C}");
                public static readonly string Link = "Link";
                public static readonly ID ImageID = new ID("{A99181A3-E6FE-4660-B740-6EE01876845F}");
                public static readonly string Image= "Image";
                public static readonly ID AltTextD = new ID("{1ED6EBD9-68E7-469C-9A22-C1D13B4F4A44}");
                public static readonly string AltText = "AltText";
            }
        }
    }
}