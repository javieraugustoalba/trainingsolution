﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TrainingSolution.Feature.PageContent.Models;

namespace TrainingSolution.Feature.PageContent.Repositories
{
    public interface IPageContentRepository
    {
        List<SectionsModel> GetSections();
    }
}