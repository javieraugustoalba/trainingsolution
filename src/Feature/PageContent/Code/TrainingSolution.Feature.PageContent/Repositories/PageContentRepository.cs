﻿using Sitecore.Foundation.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TrainingSolution.Feature.PageContent.Models;

namespace TrainingSolution.Feature.PageContent.Repositories
{
    [Service(typeof(IPageContentRepository))]
    public class PageContentRepository : IPageContentRepository
    {
        public List<SectionsModel> GetSections()
        {
            var listSectionModel = new List<SectionsModel>();
            var model = new SectionsModel();
            try
            {
                var sectionsListFolder = Sitecore.Context.Database.GetItem(Constants.SECTIONS_FOLDER_ID);
                var sectionList = sectionsListFolder.GetChildren().Where(m => m.TemplateID == Templates.Section.ID);
                if (sectionList != null && sectionList.Count() > 0)
                {
                    foreach (var sectionItem in sectionList)
                    {
                        model = new SectionsModel()
                        {
                            Link = sectionItem.Fields[Templates.Section.Fields.Link].ToString(),
                            Title = sectionItem.Fields[Templates.Section.Fields.Title].ToString(),
                            Image = sectionItem.Fields[Templates.Section.Fields.Image].ToString(),
                            AltText = sectionItem.Fields[Templates.Section.Fields.AltText].ToString()
                        };
                        listSectionModel.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return listSectionModel;
        }
    }
}